import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Game }         from '../game';
import { GameService }  from '../game.service';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: [ './game-detail.component.css' ]
})
export class GameDetailComponent implements OnInit {
  @Input() game: Game;
  myimage1 :string ="assets/img/PD1.jpg";
  myimage2 :string ="assets/img/PD2.jpg";
  myimage3 :string ="assets/img/PD3.jpg";
  myimage4 :string ="assets/img/PD4.jpg";
  myimage5 :string ="assets/img/PD5.jpg";
  myimage6 :string ="assets/img/PD6.jpg";
  myimage7 :string ="assets/img/PD7.jpg";
  myimage8 :string ="assets/img/PD8.jpg";
  myimage9 :string ="assets/img/PD9.jpg";
  myimage10:string ="assets/img/PD10.jpg";
  myimage11:string ="assets/img/PD11.jpg";
  myimage12:string ="assets/img/PD12.jpg";
  myimage13:string ="assets/img/PD13.jpg";
  myimage14:string ="assets/img/PD14.jpg";
  myimage15:string ="assets/img/PD15.jpg";
  constructor(
    private route: ActivatedRoute,
    private gameService: GameService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getGame();
  }

  getGame(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.gameService.getGame(id) 
      .subscribe(game => this.game = game);
  }
  
  goBack(): void {
    this.location.back();
  }

 save(): void {
    this.gameService.updateGame(this.game)
      .subscribe(() => this.goBack());
  }
}