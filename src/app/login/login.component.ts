import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { LoginService } from '../login.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit{

    items;
    checkoutForm;

    constructor(
        private loginService: LoginService,
        private formBuilder: FormBuilder
    ) {
        this.items = this.loginService.getItem();
        this.checkoutForm = this.formBuilder.group({
            Username: '',
            Password: ''
        });
    }

    onSubmit(customerData){
        console.warn('Login succeeded!', customerData);
        
        this.items = this.loginService.clearLogin();
        this.checkoutForm.reset();
    }

    ngOnInit(){
        this.items = this.loginService.getItem();
    }
}