import { Game } from './game';

export const GAMES: Game[] = [
    { id: 1, name: 'Action'},
    { id: 2, name: 'Survival'},
    { id: 3, name: 'Horror'},
    { id: 4, name: 'Shooter'},
    { id: 5, name: 'Open World'}
];
