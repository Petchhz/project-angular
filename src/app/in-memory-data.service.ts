import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Game } from './game';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  myimage1 :string ="assets/img/PD1.jpg";
  myimage2 :string ="assets/img/PD2.jpg";
  myimage3 :string ="assets/img/PD3.jpg";
  myimage4 :string ="assets/img/PD4.jpg";
  myimage5 :string ="assets/img/PD5.jpg";
  myimage6 :string ="assets/img/PD6.jpg";
  myimage7 :string ="assets/img/PD7.jpg";
  myimage8 :string ="assets/img/PD8.jpg";
  myimage9 :string ="assets/img/PD9.jpg";
  myimage10:string ="assets/img/PD10.jpg";
  myimage11:string ="assets/img/PD11.jpg";
  myimage12:string ="assets/img/PD12.jpg";
  myimage13:string ="assets/img/PD13.jpg";
  myimage14:string ="assets/img/PD14.jpg";
  myimage15:string ="assets/img/PD15.jpg";
  createDb() {
    const games = [
      { id: 1, name: 'Action'},
      { id: 2, name: 'Survival'},
      { id: 3, name: 'Horror'},
      { id: 4, name: 'Shooter'},
      { id: 5, name: 'Open World'}
    ];
    return {games};
  }

  genId(games: Game[]): number {
    return games.length > 0 ? Math.max(...games.map(game => game.id)) + 1 : 11;
  }
}
