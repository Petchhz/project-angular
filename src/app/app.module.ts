import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { RouterModule } from '@angular/router';
import { AppComponent }         from './app.component';
import { HomepageComponent }   from './homepage/homepage.component';
import { GameDetailComponent }  from './game-detail/game-detail.component';
import { GamesComponent }      from './games/games.component';
import { GameSearchComponent }  from './game-search/game-search.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { TopBarComponent } from './top-bar/top-bar.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      [
        { path: '',component: HomepageComponent},
        { path: 'home', component: HomepageComponent },
        { path: 'detail/:id', component: GameDetailComponent },
        { path: 'games', component: GamesComponent },
        { path: 'register', component: RegisterComponent },
        { path: 'login', component: LoginComponent }
      ]
    ),
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  declarations: [
    AppComponent,RegisterComponent,
    HomepageComponent,LoginComponent,
    GamesComponent,TopBarComponent,
    GameDetailComponent,
    GameSearchComponent,
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
